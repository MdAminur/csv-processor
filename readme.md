# Employee Management API

This application provides a RESTful API for managing employee data. It supports operations to retrieve all employees, fetch details of a specific employee by ID, delete an employee, and import employee data from a CSV file.

## Getting Started

These instructions will guide you through the setup process and demonstrate how to use the API.

### Prerequisites

- Docker and Docker Compose
- PHP 8.3 or higher
- Composer
- Symfony CLI

### Installation


1. **Start the Database**

   The application supports PostgreSQL databases running in Docker containers.

    - To start the database, run:

        ```
        docker-compose up -d
        ```

   This command starts the necessary database services in detached mode.

3. **Install Dependencies**

   Use Composer to install the PHP dependencies:

    ```
    composer install
    ```

4. **Start the Symfony Server**

   To start the Symfony development server, run:

    ```
    symfony server:start
    ```

   Ensure the server is running on `http://localhost:8000`.

### Using the API

#### Import Employee Data

To import employee data from a CSV file, use the following `curl` command:

```
curl -X POST -H 'Content-Type: text/csv' --data-binary @import.csv http://localhost:8000/api/employee
```

classic RESTful API's for employee management:

*   `GET /api/employee`
*   `GET /api/employee/{id}`
*   `DELETE /api/employee/{id}`