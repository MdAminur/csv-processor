<?php declare(strict_types=1);

namespace App\Handler;

use App\DTO\EmployeeDTO;
use App\Entity\Employee;
use App\Exception\EmployeeNotFoundException;
use App\Repository\EmployeeRepositoryInterface;

class EmployeeHandler
{
    public function __construct(private readonly EmployeeRepositoryInterface $employeeRepository)
    {
    }

    public function handleEmployeeDTOs(array $employeeDTOs): void
    {
        ini_set('max_execution_time', '3200');
        ini_set('memory_limit', '-1');

        foreach ($employeeDTOs as $employeeDTO) {
            $employee = new Employee(
                $employeeDTO->getEmpId(),
                $employeeDTO->getUserName(),
                $employeeDTO->getNamePrefix(),
                $employeeDTO->getFirstName(),
                $employeeDTO->getMiddleInitial(),
                $employeeDTO->getLastName(),
                $employeeDTO->getGender(),
                $employeeDTO->getEmail(),
                $employeeDTO->getDateOfBirth(),
                $employeeDTO->getTimeOfBirth(),
                $employeeDTO->getAgeInYrs(),
                $employeeDTO->getDateOfJoining(),
                $employeeDTO->getAgeInCompanyYears(),
                $employeeDTO->getPhoneNo(),
                $employeeDTO->getPlaceName(),
                $employeeDTO->getCounty(),
                $employeeDTO->getCity(),
                $employeeDTO->getZip(),
                $employeeDTO->getRegion()
            );

            try {
                $this->employeeRepository->findByEmployeeId($employeeDTO->getEmpId());
            }catch (EmployeeNotFoundException)
            {
                $this->employeeRepository->save($employee);
            }
        }
    }

    public function findAllEmployee(): array
    {
        return $this->employeeRepository->findAllEmployee();
    }

    public function findByEmployeeId(int $id): Employee
    {
        return $this->employeeRepository->findByEmployeeId($id);
    }

    public function delete(int $id): void
    {
        $this->employeeRepository->delete($id);
    }
}