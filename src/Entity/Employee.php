<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTimeInterface;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity]
#[ORM\Table(name: 'employee')]
class Employee
{
    #[ORM\Id]
    #[ORM\Column(type: "uuid", unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private Uuid $id;

    #[ORM\Column(type: 'integer', unique: true)]
    private int $employeeId;

    #[ORM\Column(type: 'string', length: 255)]
    private string $userName;

    #[ORM\Column(type: 'string', length: 10)]
    private string $namePrefix;

    #[ORM\Column(type: 'string', length: 255)]
    private string $firstName;

    #[ORM\Column(type: 'string', length: 1)]
    private string $middleInitial;

    #[ORM\Column(type: 'string', length: 255)]
    private string $lastName;

    #[ORM\Column(type: 'string', length: 1)]
    private string $gender;

    #[ORM\Column(type: 'string', length: 255)]
    private string $email;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $dateOfBirth;

    #[ORM\Column(type: 'time', nullable: true)]
    private ?DateTimeInterface $timeOfBirth;

    #[ORM\Column(type: 'float')]
    private float $ageInYrs;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $dateOfJoining;

    #[ORM\Column(type: 'float')]
    private float $ageInCompanyYears;

    #[ORM\Column(type: 'string', length: 20)]
    private string $phoneNo;

    #[ORM\Column(type: 'string', length: 255)]
    private string $placeName;

    #[ORM\Column(type: 'string', length: 255)]
    private string $county;

    #[ORM\Column(type: 'string', length: 255)]
    private string $city;

    #[ORM\Column(type: 'integer', length: 10)]
    private int $zip;

    #[ORM\Column(type: 'string', length: 255)]
    private string $region;

    public function __construct(
        int $employeeId,
        string $userName,
        string $namePrefix,
        string $firstName,
        string $middleInitial,
        string $lastName,
        string $gender,
        string $email,
        ?DateTimeInterface $dateOfBirth,
        ?DateTimeInterface $timeOfBirth,
        float $ageInYrs,
        ?DateTimeInterface $dateOfJoining,
        float $ageInCompanyYears,
        string $phoneNo,
        string $placeName,
        string $county,
        string $city,
        int $zip,
        string $region
    ) {
        $this->id = Uuid::v4();
        $this->employeeId = $employeeId;
        $this->userName = $userName;
        $this->namePrefix = $namePrefix;
        $this->firstName = $firstName;
        $this->middleInitial = $middleInitial;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->email = $email;
        $this->dateOfBirth = $dateOfBirth;
        $this->timeOfBirth = $timeOfBirth;
        $this->ageInYrs = $ageInYrs;
        $this->dateOfJoining = $dateOfJoining;
        $this->ageInCompanyYears = $ageInCompanyYears;
        $this->phoneNo = $phoneNo;
        $this->placeName = $placeName;
        $this->county = $county;
        $this->city = $city;
        $this->zip = $zip;
        $this->region = $region;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEmployeeId(): int
    {
        return $this->employeeId;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getNamePrefix(): string
    {
        return $this->namePrefix;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getMiddleInitial(): string
    {
        return $this->middleInitial;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateOfBirth(): ?DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getTimeOfBirth(): ?DateTimeInterface
    {
        return $this->timeOfBirth;
    }

    /**
     * @return float
     */
    public function getAgeInYrs(): float
    {
        return $this->ageInYrs;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateOfJoining(): ?DateTimeInterface
    {
        return $this->dateOfJoining;
    }

    /**
     * @return float
     */
    public function getAgeInCompanyYears(): float
    {
        return $this->ageInCompanyYears;
    }

    /**
     * @return string
     */
    public function getPhoneNo(): string
    {
        return $this->phoneNo;
    }

    /**
     * @return string
     */
    public function getPlaceName(): string
    {
        return $this->placeName;
    }

    /**
     * @return string
     */
    public function getCounty(): string
    {
        return $this->county;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getZip(): int
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }
}