<?php declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

class EmployeeNotFoundException extends RuntimeException
{
    public function __construct($message = 'Employee not found', $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
