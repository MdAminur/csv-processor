<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Employee;

interface EmployeeRepositoryInterface
{
    public function save(Employee $product) : void;

    public function findByEmployeeId(int $employeeId) : Employee;

    public function findAllEmployee() : array;

    public function delete(int $id): void;

}