<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Employee;
use App\Exception\EmployeeNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EmployeeRepository extends ServiceEntityRepository implements EmployeeRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employee::class);
    }

    public function save(Employee $product): void
    {
        $em = $this->getEntityManager();
        $em->persist($product);
        $em->flush();
    }

    public function delete(int $id): void
    {
        $employee = $this->findByEmployeeId($id);

        $em = $this->getEntityManager();
        $em->remove($employee);
        $em->flush();
    }


    public function findByEmployeeId(int $employeeId) : Employee
    {
        $employee = $this->findOneBy(['employeeId' => $employeeId]);

        if (!$employee instanceof Employee)
        {
            throw new EmployeeNotFoundException();
        }

        return $employee;
    }

    public function findAllEmployee() : array
    {
        return $this->createQueryBuilder('e')
            ->orderBy('e.lastName', 'ASC')
            ->getQuery()
            ->getResult();    }
}