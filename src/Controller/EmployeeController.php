<?php declare(strict_types=1);

namespace App\Controller;

use App\Exception\EmployeeNotFoundException;
use App\Handler\EmployeeHandler;
use App\Parser\CSVParser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class EmployeeController extends AbstractController
{
    public function __construct
    (
        private readonly EmployeeHandler $employeeHandler
    )
    {
    }

    #[Route('/api/employee', name: 'employee_import', methods: ['POST'])]
    public function employeeImport(Request $request): Response
    {
        $data = $request->getContent();

        if (empty($data)) {
            return $this->json(['error' => 'No CSV file provided'], Response::HTTP_BAD_REQUEST);
        }
        $this->employeeHandler->handleEmployeeDTOs(CSVParser::createEmployeeDTOsFromCSVContent($data));
        return $this->json("employee data saved !!!");
    }

    #[Route('/api/employee', name: 'employee_list', methods: ['GET'])]
    public function employeeList(): Response
    {
        return $this->json($this->employeeHandler->findAllEmployee(), Response::HTTP_OK,['Content-Type' => 'application/json']);
    }

    #[Route('/api/employee/{id}', name: 'get_employee', methods: ['GET'])]
    public function getEmployee(int $id): Response
    {
        try {
            return $this->json($this->employeeHandler->findByEmployeeId($id), Response::HTTP_OK,['Content-Type' => 'application/json']);
        }catch (EmployeeNotFoundException)
        {
            $message = sprintf("Employee with [ID = %d] does not exist", $id);
            return $this->json($message, Response::HTTP_NOT_FOUND,['Content-Type' => 'application/json']);
        }
    }

    #[Route('/api/employee/{id}', name: 'delete_employee', methods: ['DELETE'])]
    public function deleteEmployee(int $id): Response
    {
        try {
            $this->employeeHandler->delete($id);
        }catch (EmployeeNotFoundException)
        {
            $message = sprintf("Employee with [ID = %d] does not exist", $id);
            return $this->json($message, Response::HTTP_NOT_FOUND,['Content-Type' => 'application/json']);
        }

        $message = sprintf("Employee with [ID = %d] deleted", $id);

        return $this->json($message, Response::HTTP_OK,['Content-Type' => 'application/json']);
    }

}