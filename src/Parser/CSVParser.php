<?php declare(strict_types=1);

namespace App\Parser;

use App\DTO\EmployeeDTO;

class CSVParser
{
    public static function createEmployeeDTOsFromCSVContent(string $csvData): array
    {
        $employeeDataAsArray = self::csvToArray($csvData);
        $employeeDTOs = [];
        foreach ($employeeDataAsArray as $employee) {
            $employeeDTOs[] = EmployeeDTO::createFromArray($employee);
        }
        return $employeeDTOs;
    }

    private static function csvToArray($csvData): array
    {
        $lines = explode(PHP_EOL, $csvData);
        $header = null;
        $data = [];

        foreach ($lines as $line) {
            if (empty($line)) {
                continue;
            }
            if (!$header) {
                $header = str_getcsv($line);
            } else {
                $row = str_getcsv($line);
                if (count($header) === count($row)) {
                    $data[] = array_combine($header, $row);
                }
            }
        }

        return $data;
    }
}