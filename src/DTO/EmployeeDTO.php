<?php declare(strict_types=1);

namespace App\DTO;

class EmployeeDTO
{
    private int $empId;
    private string $namePrefix;
    private string $firstName;
    private string $middleInitial;
    private string $lastName;
    private string $gender;
    private string $email;
    private ?\DateTime $dateOfBirth;
    private ?\DateTime $timeOfBirth;
    private float $ageInYrs;
    private ?\DateTime $dateOfJoining;
    private float $ageInCompanyYears;
    private string $phoneNo;
    private string $placeName;
    private string $county;
    private string $city;
    private int $zip;
    private string $region;
    private string $userName;

    private function __construct(
        int $empId,
        string $namePrefix,
        string $firstName,
        string $middleInitial,
        string $lastName,
        string $gender,
        string $email,
        ?\DateTime $dateOfBirth,
        ?\DateTime $timeOfBirth,
        float $ageInYrs,
        ?\DateTime $dateOfJoining,
        float $ageInCompanyYears,
        string $phoneNo,
        string $placeName,
        string $county,
        string $city,
        int $zip,
        string $region,
        string $userName
    ) {
        $this->empId = $empId;
        $this->namePrefix = $namePrefix;
        $this->firstName = $firstName;
        $this->middleInitial = $middleInitial;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->email = $email;
        $this->dateOfBirth = $dateOfBirth;
        $this->timeOfBirth = $timeOfBirth;
        $this->ageInYrs = $ageInYrs;
        $this->dateOfJoining = $dateOfJoining;
        $this->ageInCompanyYears = $ageInCompanyYears;
        $this->phoneNo = $phoneNo;
        $this->placeName = $placeName;
        $this->county = $county;
        $this->city = $city;
        $this->zip = $zip;
        $this->region = $region;
        $this->userName = $userName;
    }

    public function getEmpId(): int { return $this->empId; }
    public function getNamePrefix(): string { return $this->namePrefix; }
    public function getFirstName(): string { return $this->firstName; }
    public function getMiddleInitial(): string { return $this->middleInitial; }
    public function getLastName(): string { return $this->lastName; }
    public function getGender(): string { return $this->gender; }
    public function getEmail(): string { return $this->email; }
    public function getDateOfBirth(): ?\DateTime { return $this->dateOfBirth; }
    public function getTimeOfBirth(): ?\DateTime { return $this->timeOfBirth; }
    public function getAgeInYrs(): float { return $this->ageInYrs; }
    public function getDateOfJoining(): ?\DateTime { return $this->dateOfJoining; }
    public function getAgeInCompanyYears(): float { return $this->ageInCompanyYears; }
    public function getPhoneNo(): string { return $this->phoneNo; }
    public function getPlaceName(): string { return $this->placeName; }
    public function getCounty(): string { return $this->county; }
    public function getCity(): string { return $this->city; }
    public function getZip(): int { return $this->zip; }
    public function getRegion(): string { return $this->region; }
    public function getUserName(): string { return $this->userName; }

    public static function createFromArray(array $data): self
    {
        $empId = (int) $data['Emp ID'];
        $namePrefix = $data['Name Prefix'];
        $firstName = $data['First Name'];
        $middleInitial = $data['Middle Initial'];
        $lastName = $data['Last Name'];
        $gender = $data['Gender'];
        $email = $data['E Mail'];

        $dateOfBirth = \DateTime::createFromFormat('m/d/Y', $data['Date of Birth']) ?: null;
        $timeOfBirth = \DateTime::createFromFormat('h:i:s A', $data['Time of Birth']) ?: null;
        $dateOfJoining = \DateTime::createFromFormat('m/d/Y', $data['Date of Joining']) ?: null;

        $ageInYrs = (float) $data['Age in Yrs.'];
        $ageInCompanyYears = (float) $data['Age in Company (Years)'];
        $phoneNo = $data['Phone No. '];
        $placeName = $data['Place Name'];
        $county = $data['County'];
        $city = $data['City'];
        $zip = (int) $data['Zip'];
        $region = $data['Region'];
        $userName = $data['User Name'];

        return new self(
            empId: $empId,
            namePrefix: $namePrefix,
            firstName: $firstName,
            middleInitial: $middleInitial,
            lastName: $lastName,
            gender: $gender,
            email: $email,
            dateOfBirth: $dateOfBirth,
            timeOfBirth: $timeOfBirth,
            ageInYrs: $ageInYrs,
            dateOfJoining: $dateOfJoining,
            ageInCompanyYears: $ageInCompanyYears,
            phoneNo: $phoneNo,
            placeName: $placeName,
            county: $county,
            city: $city,
            zip: $zip,
            region: $region,
            userName: $userName
        );
    }

}
